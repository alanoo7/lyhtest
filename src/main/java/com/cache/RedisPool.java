package com.cache;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.utils.Utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;







import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

public class RedisPool implements InitializingBean, DisposableBean{
	
	private int maxActive = 400;
	private int maxIdle = 20;
	private int maxWait = 10000;
	private static JedisPool pool;
	private String redisNodes="127.0.0.1";
	private String redisPort="6379";
	private Logger logger = Logger.getLogger(getClass());

	@Override
	public void destroy() throws Exception {
		pool.destroy();
	}
	
	public RedisPool(String port,String redisNodes){
		if(port!=null)this.redisPort = port;
		if(redisNodes!=null)this.redisNodes = redisNodes;
		init();
	}
	
	public RedisPool(){
		preporeInit();
		init();
	}
	
	public void preporeInit(){
		InputStream in = RedisCache.class.getClassLoader()
				.getResourceAsStream("redis.properties");
		Properties properties = new Properties();
		try {
			properties.load(in);
			this.redisPort = properties.getProperty("jedis_port");
			this.redisNodes = properties.getProperty("jedis_host");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxIdle(maxIdle);
		config.setTestOnBorrow(true);
		config.setTestOnReturn(true);
		try {
			logger.info("初始化redispool:redisNodes:"+redisNodes+";redisPort:"+redisPort);
			if(StringUtils.isNotBlank(redisNodes) && StringUtils.isNotBlank(redisPort)){
				if(Utils.isNumber(redisPort)){
					redisPort = redisPort.trim();
					pool = new JedisPool(config, redisNodes,Integer.parseInt(redisPort));
					logger.info("初始化redispool成功！redisPort:"+redisPort);
				}else{
					pool = new JedisPool(config, redisNodes);
					logger.info("初始化redispool成功！redisPort:"+Protocol.DEFAULT_PORT);
				}
			    
			}else {
				pool = new JedisPool(config, redisNodes);
				logger.info("初始化redispool成功！redisPort:"+Protocol.DEFAULT_PORT);
			}
			
		} catch (Exception e) {
			logger.error("Redis缓存初始化",e);
		}
	}
	
	public  void init(){
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxIdle(maxIdle);
		config.setTestOnBorrow(true);
		config.setTestOnReturn(true);
		try {
			logger.info("初始化redispool:redisNodes:"+redisNodes+";redisPort:"+redisPort);
			if(StringUtils.isNotBlank(redisNodes) && StringUtils.isNotBlank(redisPort)){
				if(Utils.isNumber(redisPort)){
					redisPort = redisPort.trim();
					pool = new JedisPool(config, redisNodes,Integer.parseInt(redisPort));
					logger.info("初始化redispool成功！redisPort:"+redisPort);
				}else{
					pool = new JedisPool(config, redisNodes);
					logger.info("初始化redispool成功！redisPort:"+Protocol.DEFAULT_PORT);
				}
			    
			}else {
				pool = new JedisPool(config, redisNodes);
				logger.info("初始化redispool成功！redisPort:"+Protocol.DEFAULT_PORT);
			}
			
		} catch (Exception e) {
			logger.error("Redis缓存初始化",e);
		}
	}

	public void setMaxActive(int maxActive) {
		this.maxActive = maxActive;
	}

	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}

	public void setMaxWait(int maxWait) {
		this.maxWait = maxWait;
	}

	public JedisPool getPool() {
		return pool;
	}

	public void setRedisNodes(String redisNodes) {
		this.redisNodes = redisNodes;
	}
	public String getRedisPort() {
		return redisPort;
	}

	public void setRedisPort(String redisPort) {
		this.redisPort = redisPort;
	}
	
	public void setPool(JedisPool pool) {
		this.pool = pool;
	}


	public static Jedis getJedis() { 
		Jedis jedis = null;
		if(pool != null){
			jedis = pool.getResource();
		}
		return jedis;
	}
}