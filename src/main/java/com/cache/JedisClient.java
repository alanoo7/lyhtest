package com.cache;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

public class JedisClient{
	
	private JedisPool pool;
	
	public String get(String key) {
		return get(-1,key, new TypeReference<String>(){});
	}
	
	public String get(int db, String key) {
		return get(db, key, new TypeReference<String>(){});
	}
	
	public <T> T get(String key, TypeReference<T> tr) {
		return get(-1, key, tr);
	}
	
	public <T> T get(int db, String key, TypeReference<T> tr) {
		
		if(StringUtils.isBlank(key)){
			return null;
		}
		
		T result = null;
		
		Jedis redis = null;
		
		boolean borrowOrOprSuccess = true;
		
		try {
			redis = pool.getResource();
			
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}

			String value = redis.get(key);
			
			if(StringUtils.isBlank(value)){
				return null;
			}
			
			return (T) JSON.parseObject(value, tr);
				
		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);
 
		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
				
		}
		
		return result;
	}
	
	
	public <T> boolean set(String key, T value) {
		return set(key, 0, value);
	}

	public <T> boolean set(String key, int TTL, T value) {
		return set(-1, key, TTL, value);
	}
	
	
	public <T> boolean set(int db, String key, int TTL, T value) {
		
		if(value == null){
			return false;
		}
		
		Jedis redis = null;
		
		boolean borrowOrOprSuccess = true;
		
		try {
			redis = pool.getResource();
			
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}
			
			if(TTL == 0){
				redis.set(key, JSON.toJSONString(value));
			}else {
				redis.setex(key, TTL, JSON.toJSONString(value));
			}
			
		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);
			
			return false;
 
		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}
		
		return true;
	}

	public boolean delete(String key) {
		return delete(-1,key);
	}
	
	public boolean delete(int db, String key) {
		
		if(StringUtils.isBlank(key)){
			return false;
		}
		
		Jedis redis = null;
		
		boolean borrowOrOprSuccess = true;
		
		try {
			redis = pool.getResource();
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}
			redis.del(key);
			
		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);
			
			return false;
 
		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}
		
		return true;
		
	}

	public boolean expire(String key, int TTL) {
		return expire(-1, key, TTL);
	}
	
	public boolean expire(int db,String key, int TTL) {
		Jedis redis = null;
		
		boolean borrowOrOprSuccess = true;
		
		try {
			redis = pool.getResource();
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}
			redis.expire(key, TTL);
			
		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);
		} finally {
			if (borrowOrOprSuccess){
				redis.select(0);
				pool.returnResource(redis);
			}
		}
		
		return true;
	}
	
	public String hget(int db,String key, String field) {

		if (StringUtils.isBlank(key) || StringUtils.isBlank(field)) {
			return null;
		}

		String result = null;

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}

			return redis.hget(key, field);

		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}

		return result;
	}
	
	public String hget(String key, String field) {
		return hget(-1, key,field);
	}
	
	public byte[] hget(int db, byte[] key, byte[] field) {

		if (key == null || key.length == 0 || field == null || field.length == 0) {
			return null;
		}

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}

			return redis.hget(key, field);

		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}

		return null;
	}
	
	public byte[] hget(byte[] key, byte[] field) {
		return hget(-1, key, field);
	}
	
	public int hsetnx(int db,String key, String field, String value) {

		if (StringUtils.isBlank(key) || StringUtils.isBlank(field)) {
			return 0;
		}

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}

			return redis.hsetnx(key, field, value).intValue();

		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}

		return 0;
	}
	
	public int hsetnx(String key, String field, String value) {
		return hsetnx(-1, key, field, value);
	}
	
	public int hset(int db, byte[] key, byte[] field, byte[] value) {

		if (key == null || key.length ==0 || field == null || field.length == 0 || value == null || value.length == 0) {
			return -1;
		}

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}

			return redis.hset(key, field, value).intValue();

		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}

		return -1;
	}
	
	public int hset(byte[] key, byte[] field, byte[] value) {
		return hset(-1, key, field, value);
	}
	
	public Map<String, String> hgetAll(String key) {

		return hgetAll(-1, key);
	}
	
	public Map<String, String> hgetAll(int db,String key) {

		if (StringUtils.isBlank(key)) {
			return null;
		}

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}

			return redis.hgetAll(key);

		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
				
		}

		return null;
	}
	
	public Map<byte[],byte[]> hgetAll(int db,byte[] key) {

		if (key == null || key.length == 0) {
			return null;
		}

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}

			return redis.hgetAll(key);

		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}

		return null;
	}
	
	public Map<byte[],byte[]> hgetAll(byte[] key) {

		return hgetAll(-1, key);
	}

	public int hset(String key, String field, String value) {
		return hset(-1, key, field, value);
	}
	
	public int hset(int db,String key, String field, String value) {

		if (StringUtils.isBlank(key) || StringUtils.isBlank(field)
				|| StringUtils.isBlank(value)) {
			return -1;
		}

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}
			return redis.hset(key, field, value).intValue();
		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

			return -1;

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}
	}
	
	public Set<String> hkeys(String key) {
		return hkeys(-1,key);
	}
	
	public Set<String> hkeys(int db,String key) {

		if (StringUtils.isBlank(key)) {
			return new HashSet<String>();
		}

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}
			return redis.hkeys(key);
		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

			return new HashSet<String>();

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}
	}

	public boolean hdel(int db,String key, String field) {

		if (StringUtils.isBlank(key) || StringUtils.isBlank(field)) {
			return false;
		}

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}
			redis.hdel(key, field);

		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

			return false;

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}

		return true;

	}
	
	public boolean hdel(String key, String field) {
		return hdel(-1, key, field);
	}
	
	public boolean hexists(String key, String field) {
		return hexists(-1, key, field);
	}
	
	public boolean hexists(int db,String key, String field) {

		if (StringUtils.isBlank(key) || StringUtils.isBlank(field)) {
			return false;
		}

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}
			return redis.hexists(key, field);

		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

			return false;

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}
	}
	
	public int setnx(String key, String value) {
		return setnx(-1, key, value);
	}
	
	public int setnx(int db,String key, String value) {

		if (StringUtils.isBlank(key) || StringUtils.isBlank(value)) {
			return -1;
		}

		Jedis redis = null;

		boolean borrowOrOprSuccess = true;

		try {
			redis = pool.getResource();
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}
			return redis.setnx(key, value).intValue();

		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);

			return 0;

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}
	}
	
	public Set<String> keys(String pattern) {
		return keys(-1, pattern);
	}
	
	public Set<String> keys(int db,String pattern) {
		Jedis redis = null;
		Set<String> keysSet = new HashSet<String>();
		
		boolean borrowOrOprSuccess = true;
		
		try {
			redis = pool.getResource();
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}
			keysSet = redis.keys(pattern);
			
		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);
		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(redis);
			}
		}
		
		return keysSet;
	}
	
//	public long publish(String channel, String message){
//		Jedis redis = null;
//		
//		boolean borrowOrOprSuccess = true;
//		
//		try {
//			redis = pool.getResource();
//			return redis.publish(channel, message);
//			
//		} catch (Exception e) {
//			borrowOrOprSuccess = false;
//			if (redis != null)
//				pool.returnBrokenResource(redis);
//		} finally {
//			if (borrowOrOprSuccess)
//				pool.returnResource(redis);
//		}
//		
//		return -1;
//	}
//	
//	public void subscribe(JedisPubSub pubSub, String... channel){
//		Jedis redis = null;
//		
//		boolean borrowOrOprSuccess = true;
//		
//		try {
//			redis = pool.getResource();
//			redis.subscribe(pubSub, channel);
//		} catch (Exception e) {
//			borrowOrOprSuccess = false;
//			if (redis != null)
//				pool.returnBrokenResource(redis);
//		} finally {
//			if (borrowOrOprSuccess)
//				pool.returnResource(redis);
//		}
//	}
//	
//	public void psubscribe(JedisPubSub pubSub, String... patterns){
//		Jedis redis = null;
//		
//		boolean borrowOrOprSuccess = true;
//		
//		try {
//			redis = pool.getResource();
//			redis.psubscribe(pubSub, patterns);
//		} catch (Exception e) {
//			borrowOrOprSuccess = false;
//			if (redis != null)
//				pool.returnBrokenResource(redis);
//		} finally {
//			if (borrowOrOprSuccess)
//				pool.returnResource(redis);
//		}
//	}
	
	public long ttl(int db, String key) {
		if(StringUtils.isBlank(key)){
			return 0;
		}
		
		Jedis redis = null;
		
		boolean borrowOrOprSuccess = true;
		
		try {
			redis = pool.getResource();
			if(db >= 0){
				redis.select(db);
			}else {
				redis.select(0);
			}
			return redis.ttl(key.getBytes());
			
		} catch (Exception e) {
			borrowOrOprSuccess = false;
			if (redis != null)
				pool.returnBrokenResource(redis);
			
			return 0;
 
		} finally {
			if (borrowOrOprSuccess)
				pool.returnResource(redis);
		}
	}

	public void setPool(RedisPool pool) {
		this.pool = pool.getPool();
	}
}