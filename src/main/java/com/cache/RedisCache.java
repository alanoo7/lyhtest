package com.cache;

import java.io.InputStream;
import java.util.Properties;

import org.springframework.stereotype.Component;

import com.utils.SerializeUtil;

import redis.clients.jedis.Jedis;

/**
 * RedisCache : redis 缓存 插件
 */
@Component(value = "redisCache")
public class RedisCache {
	public Jedis getjedis() {
		InputStream in = RedisCache.class.getClassLoader()
				.getResourceAsStream("redis.properties");
		Properties properties = new Properties();
		try {
			properties.load(in);
			String host=properties.getProperty("redis.host");
			int port=Integer.parseInt(properties.getProperty("redis.port"));
			Jedis jedis = new Jedis(host, port);
			return jedis;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String cache(String key, Object object, int seconds) {
		Jedis jedis =getjedis();
		String result = jedis.set(key.getBytes(), SerializeUtil.serialize(object));
		jedis.expire(key, seconds);
		return result;
	}

	public Object get(String key) {
		Jedis jedis =getjedis();
		byte[] bytesData = jedis.get((key).getBytes());
		return SerializeUtil.unserialize(bytesData);
	}

	public void expirse(String key, int seconds) {
		Jedis jedis =getjedis();
		jedis.expire(key, seconds);
	}
}
