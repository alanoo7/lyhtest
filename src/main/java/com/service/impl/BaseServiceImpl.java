package com.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.BaseInfoMapper;
import com.model.BaseInfo;
import com.model.User;
import com.service.BaseService;


@Service("baseService")
public class BaseServiceImpl implements BaseService {
	
	@Resource  
    private BaseInfoMapper baseInfoMapper; 
	
	@Resource
	private SqlSessionFactory sqlSessionFactory;
	
	@Override
	public BaseInfo selectMap() {
		return (BaseInfo) baseInfoMapper.selectMap();
	}

	@Override
	public User selectByPrimaryKey(String id) {
		return baseInfoMapper.selectByPrimaryKey(id);
	}

	@Override
	public void insertUser(User user) {
		baseInfoMapper.insertUser(user);
	}

	@Override
	public List selectMap(BaseInfo info) {
		info.load();
		return baseInfoMapper.selectMap(info);
	}

	@Override
	public List selectSingle(BaseInfo info) {
		info.load();
		return baseInfoMapper.selectSingle(info);
	}

	@Override
	public Object selectOne(BaseInfo info) {
		info.load();
		return baseInfoMapper.selectOne(info);
	}

	@Override
	public boolean insert(BaseInfo info) throws Exception {
		info.load();
		int i = baseInfoMapper.insert(info);
		return i > 0 ? true : false;
	}

	@Override
	public boolean update(BaseInfo info) throws Exception {
		info.load();
		int i = baseInfoMapper.update(info);
		return i > 0 ? true : false;
	}

	@Override
	public boolean delete(BaseInfo info) throws Exception {
		info.load();
		int i = baseInfoMapper.delete(info);
		return i > 0 ? true : false;
	}

	@Override
	@Transactional
	public void execute(List<BaseInfo> list) throws Exception {
		for (BaseInfo info : list) {
			info.load();
			 baseInfoMapper.update(info);
		}
	}
	
}
