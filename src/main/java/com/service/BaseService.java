package com.service;


import java.util.List;

import com.model.BaseInfo;
import com.model.User;


public interface BaseService {
	
	public BaseInfo selectMap();
	
	public User selectByPrimaryKey(String id);
	
	public void insertUser(User user);
	
	
	public List selectMap(BaseInfo info);

	public List selectSingle(BaseInfo info);

	public Object selectOne(BaseInfo info);

	public boolean insert(BaseInfo info) throws Exception;

	public boolean update(BaseInfo info) throws Exception;

	public boolean delete(BaseInfo info) throws Exception;

	public void execute(List<BaseInfo> list) throws Exception;
}
