package com.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.BaseInfo;
import com.model.User;
import com.service.BaseService;
import com.suniot.people.dubbo.PeopleService;

@Controller
public class HelloWorldController {
	Logger logger = Logger.getLogger(HelloWorldController.class);
	
	@Autowired
	private BaseService baseService;
	
	@Autowired
	private PeopleService peopleService;
	
	@RequestMapping("/hello")
    public ModelAndView hello(HttpServletRequest request) {
         Map<String, Object> map = new HashMap<String, Object>();
         map.put("userName", "Hello World");
         logger.info("here ! here !");
         return new ModelAndView("/hello",map);
    }
	
	@RequestMapping("/test")
	public ModelAndView test(){
		List list = peopleService.getAllUsers();
		if(!list.isEmpty()){
			System.out.println(list);
		}
		Map<String, Object> map = new HashMap<String, Object>();
        logger.info("here ! here !");
        return new ModelAndView("/hello",map);
	}
}
