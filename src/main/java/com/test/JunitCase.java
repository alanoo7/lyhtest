package com.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.BaseInfo;
import com.model.Pager;
import com.model.User;
import com.service.BaseService;

public class JunitCase {
	
	private BaseService baseService ;
	@Before
	public void before() {
		ApplicationContext ac = new ClassPathXmlApplicationContext(
				new String[] { "spring.xml", "spring-mvc.xml",
						"spring-mybatis.xml" });
		baseService = (BaseService) ac.getBean("baseService");
	}

	@Test
	public void testInsert(){
		User user = new User();
		//user.setCatId("20");
		baseService.insertUser(user);
	}
	
	@Test
	public void testSelect(){
		User user = new User();
		String id = "20";
		user = baseService.selectByPrimaryKey(id);
		print(user);
	}
	
	@Test
	public void testBaseInfo(){
		BaseInfo baseInfo = new BaseInfo();
		baseInfo.setSql("select * from test");
		List list = baseService.selectMap(baseInfo);
		if(!list.isEmpty()){
			System.out.println(list);
		}
		
	}
	
	@Test
	public void test() {
		fail("Not yet implemented");
	}

	private void fail(String message) {
		System.out.println(message);
	}

	private void print(Object obj){
		System.out.println(obj.toString());
	}
}
