package com.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.redis.connection.jredis.JredisPool;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import com.cache.RedisPool;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:spring.xml","classpath:spring-mvc.xml","classpath:spring-mybatis.xml"}) // 加载配置
public class TestJedisPool {

	@Before
	public void before(){
		print("now prepare !");
	}

	private void print(Object obj) {
		System.out.println(obj.toString());
	}
	
	@Test
	public void testJedisPool(){
		RedisPool pool = new RedisPool("6379","127.0.0.1");
		Jedis jedis =  pool.getJedis();
		jedis.set("name", "liyaohua");
		print(jedis.get("name"));
	}
	
	@Test
	public void testJedisPool2(){
		RedisPool pool = new RedisPool();
		Jedis jedis =  pool.getJedis();
		jedis.set("name", "liyaohua");
		print(jedis.get("name"));
	}
	
}

